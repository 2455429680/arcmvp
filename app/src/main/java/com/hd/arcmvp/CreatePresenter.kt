package com.hd.arcmvp

import java.lang.annotation.Inherited
import kotlin.annotation.AnnotationRetention

import kotlin.reflect.KClass

@Inherited()
@Retention(AnnotationRetention.RUNTIME)
annotation class CreatePresenter(val value: KClass<out BaseMvpPresenter<*>>)