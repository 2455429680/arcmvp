package com.hd.arcmvp

import android.os.Bundle
import com.hd.arcmvp.view.BaseMvpView

class BaseMvpProxy<V : BaseMvpView, P : BaseMvpPresenter<V>>(var mFactory: PresenterMvpFactory<V, P>?) : PresenterProxyInterface<V, P> {
    companion object {
        val PRIMARY_KEY = "presenter_key"
    }

    var mPresenter: P? = null
    var mBundle: Bundle? = null
    var mIsViewAttached = false

    override fun setPresenterMvpFactory(presenterMvpFactory: PresenterMvpFactory<V, P>) {
       if (mPresenter!=null){
           throw IllegalStateException("此方法只能在presenter被创建前调用")
       }
        this.mFactory = presenterMvpFactory
    }

    override fun getMvpPresenter(): P? {
        if (mPresenter == null) {
            mPresenter = mFactory?.createMvpPresenter()
            mPresenter?.onCreatePresenter(mBundle?.getBundle(PRIMARY_KEY))
        }
        return mPresenter
    }

    override fun getPresenterMvpFactory(): PresenterMvpFactory<V, P>?= mFactory
    public fun onResume(view: V) {
        getMvpPresenter()
        takeIf { mPresenter != null && !mIsViewAttached }.apply {
            mPresenter?.onAttachMvpView(view)
            mIsViewAttached = true
        }
    }

    public fun onDetachMvpView() {
        mPresenter?.onDetachMvpView()
    }

    public fun onDestory() {
        onDetachMvpView()
        mPresenter?.onDestroyMvpPresenter()
        mPresenter = null
    }

    public fun onRestoreInstanceState(bundle: Bundle?) {
        mBundle = bundle
    }

    public fun onSaveInstanceState(): Bundle? {
        getMvpPresenter()
        mPresenter?.also {
            val presenterBundle = Bundle()
            it.onSaveInstanceState(presenterBundle)
            mBundle?.putBundle(PRIMARY_KEY, presenterBundle)
        }
        return mBundle
    }
}