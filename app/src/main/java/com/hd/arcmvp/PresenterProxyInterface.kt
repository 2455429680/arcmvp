package com.hd.arcmvp

import com.hd.arcmvp.view.BaseMvpView

interface PresenterProxyInterface<V : BaseMvpView, P : BaseMvpPresenter<V>> {
    fun getPresenterMvpFactory(): PresenterMvpFactory<V, P>?
    fun setPresenterMvpFactory(presenterMvpFactory: PresenterMvpFactory<V, P>)
    fun getMvpPresenter():P?
}