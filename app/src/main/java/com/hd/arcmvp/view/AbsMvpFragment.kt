package com.hd.arcmvp.view


import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hd.arcmvp.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */

abstract class AbsMvpFragment<V : BaseMvpView, P : BaseMvpPresenter<V>> : Fragment(), PresenterProxyInterface<V, P> {

    lateinit var mActivity: Activity
    val mProxy = BaseMvpProxy<V, P>(PresenterMvpFactoryImpl.createFactory(javaClass))
    val FRAGMENT_PRESENTER_SAVE_KEY = "fragment_presenter_save_key"
    lateinit var mRootView: View
    override fun getPresenterMvpFactory(): PresenterMvpFactory<V, P>? {
        return mProxy.getPresenterMvpFactory()
    }

    override fun setPresenterMvpFactory(presenterMvpFactory: PresenterMvpFactory<V, P>) {
        mProxy.setPresenterMvpFactory(presenterMvpFactory)
    }

    override fun getMvpPresenter(): P? {
        return mProxy.getMvpPresenter()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mActivity = activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) {
            val presenterBundle = savedInstanceState.getBundle(FRAGMENT_PRESENTER_SAVE_KEY)
            presenterBundle?.run { mProxy.onRestoreInstanceState(presenterBundle) }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (mRootView == null)
            mRootView = inflater.inflate(getLayoutRes(), container, false)
        setUpView(savedInstanceState)
        loadData(true)
        return mRootView

    }

    abstract fun setUpView(savedInstanceState: Bundle?)

    abstract fun getLayoutRes(): Int

    override fun onResume() {
        super.onResume()
        mProxy.onResume(this as V)
    }

    override fun onDestroy() {
        super.onDestroy()
        mProxy.onDestory()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mProxy.onDetachMvpView()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putBundle(FRAGMENT_PRESENTER_SAVE_KEY, mProxy.onSaveInstanceState())
    }

    abstract fun loadData(isFirst: Boolean)

}
