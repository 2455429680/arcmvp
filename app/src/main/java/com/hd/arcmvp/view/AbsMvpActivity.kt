package com.hd.arcmvp.view

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.hd.arcmvp.*

abstract class AbsMvpActivity<V : BaseMvpView, P : BaseMvpPresenter<V>> : AppCompatActivity(), PresenterProxyInterface<V, P> {
    val mProxy = BaseMvpProxy<V, P>(PresenterMvpFactoryImpl.createFactory(javaClass))
    val PRESENTER_SAVE_KEY="presenter_save_key"
    override fun getPresenterMvpFactory(): PresenterMvpFactory<V, P>? =
            mProxy.getPresenterMvpFactory()

    override fun setPresenterMvpFactory(presenterMvpFactory: PresenterMvpFactory<V, P>) {
        mProxy.setPresenterMvpFactory(presenterMvpFactory)
    }

    override fun getMvpPresenter(): P? = mProxy.getMvpPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.run {
            mProxy.onRestoreInstanceState(savedInstanceState.getBundle(PRESENTER_SAVE_KEY))
        }
    }

    override fun onResume() {
        super.onResume()
        mProxy.onResume(this as V)
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState?.putBundle(PRESENTER_SAVE_KEY,mProxy.onSaveInstanceState())
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mProxy.onDetachMvpView()
    }

    override fun onDestroy() {
        super.onDestroy()
        mProxy.onDestory()
    }


}