package com.hd.arcmvp.view


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hd.arcmvp.BaseMvpPresenter
import com.hd.arcmvp.R


abstract class AbsLazyMvpFragemnt<V : BaseMvpView, P : BaseMvpPresenter<V>> : AbsMvpFragment<V, P>() {

    var mIsVisible: Boolean = false
    var isPrepared = false
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        mIsVisible = userVisibleHint
        if (mIsVisible&&isPrepared) {
            loadData(false)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        isPrepared = true
        if (mIsVisible)
        loadData(true)
        return mRootView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        isPrepared=false
    }


}
