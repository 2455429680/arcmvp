package com.hd.arcmvp

import android.os.Bundle
import com.hd.arcmvp.view.BaseMvpView

open class BaseMvpPresenter<V : BaseMvpView> {
    var mvpView: V? = null
    open fun onAttachMvpView(view: V) {
        mvpView = view
    }

    open fun onDetachMvpView() {
        mvpView = null
    }

    //Presenter被创建后调用
    fun onCreatePresenter(savedBundle: Bundle?) {

    }
    //Prestener被销毁重建时调用
    fun onSaveInstanceState(bundle: Bundle){

    }
    fun onDestroyMvpPresenter(){

    }
}