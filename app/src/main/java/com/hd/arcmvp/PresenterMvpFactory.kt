package com.hd.arcmvp

import com.hd.arcmvp.view.BaseMvpView

interface PresenterMvpFactory<V : BaseMvpView,P:BaseMvpPresenter<V>> {
    fun createMvpPresenter():P?
}