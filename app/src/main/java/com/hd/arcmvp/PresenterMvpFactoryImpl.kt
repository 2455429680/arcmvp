package com.hd.arcmvp

import com.hd.arcmvp.view.BaseMvpView

class PresenterMvpFactoryImpl<V: BaseMvpView,P:BaseMvpPresenter<V>> private constructor(val presenterClass:Class<P>):PresenterMvpFactory<V,P> {
    override fun createMvpPresenter(): P? {

        try {
            return presenterClass.newInstance()
        }catch (e:ClassNotFoundException){
            e.printStackTrace()
        }
        return null
    }
    companion object {
        fun <V: BaseMvpView,P:BaseMvpPresenter<V>> createFactory(viewClass: Class<*>):PresenterMvpFactoryImpl<V,P>?{
            val annotation=viewClass.getAnnotation(CreatePresenter::class.java)
           val aClass=annotation?.value
            if (aClass as? Class<P>!=null){
               return PresenterMvpFactoryImpl(aClass)
           }
            return  null
        }
    }

}